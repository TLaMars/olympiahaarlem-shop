import * as React from 'react';
import { graphql, HeadFC, PageProps } from 'gatsby';
import Nav from '../components/Nav/nav';
import Header from '../components/Header/header';
import Product from '../components/Product/product';

type DataProps = {
	allContentfulShopItem: {
		nodes: [
			{
				id: string;
				price: number;
				productName: string;
				description: {
					id: string;
					internal: {
						content: string;
					};
				};
				image: {
					filename: string;
					publicUrl: string;
				};
			}
		];
	};
};

const IndexPage: React.FC<PageProps<DataProps>> = ({ data }) => {
	const { nodes } = data.allContentfulShopItem;
	console.log(nodes);
	return (
		<main>
			<Nav />
			<Header />
			<div className="products">
				{nodes.map(({ id, productName, description, image, price }) => (
					<Product
						key={id}
						title={productName}
						price={price}
						description={description.internal.content}
						image={image.publicUrl}
					/>
				))}
			</div>
		</main>
	);
};

export default IndexPage;

export const query = graphql`
	query ShopItems {
		allContentfulShopItem {
			nodes {
				id
				price
				productName
				description {
					id
					internal {
						content
					}
				}
				image {
					filename
					publicUrl
				}
			}
		}
	}
`;

export const Head: HeadFC = () => <title>OlympiaHaarlem shop</title>;
