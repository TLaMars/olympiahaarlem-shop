import React from 'react';
import './product.scss';

type Props = {
	title: string;
	image: string;
	price: number;
	description: string;
};

const Product: React.FC<Props> = ({ image, title, price, description }) => {
	return (
		<a
			className="product-container"
			href={`mailto:shop.hoso@olympiahaarlem.nl?subject=Kopen ${title}`}
		>
			<img className="image" src={image} alt={title} />
			<div className="info-section">
				<h3 className="name">{title}</h3>
				<p className="description">{description}</p>
				<h4 className="price">€{price}</h4>
			</div>
		</a>
	);
};

export default Product;
