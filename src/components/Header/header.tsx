import React from 'react';
import './header.scss';

const Header: React.FC = () => {
	return (
		<div className="header-container">
			<h1 className="title">Olympia Haarlem Shop</h1>
			<hr className="line" />
		</div>
	);
};

export default Header;
