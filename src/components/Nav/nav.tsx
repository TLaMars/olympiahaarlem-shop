import React from 'react';
import './nav.scss';

const Nav: React.FC = () => {
	return (
		<nav className="container">
			<a className="link" href="https://olympiahaarlem.nl">
				Home
			</a>
			<a className="link active" href="/">
				Shop
			</a>
		</nav>
	);
};

export default Nav;
